var server = "http://42ccd922.ngrok.io";
var dataUrl = "/api/data";
var stateUrl = "/api/state"
var refreshTime = 5000;
$(document).ready(function(){
    connect();
    setInterval(function(){
        connect();
    },refreshTime);
    $("section#server p>strong").text(server);
    $("button#change").click(function(){
        changeServer();
        
    });
    $("section#server input").on('keypress',function (e){
        if (e.key === 'Enter') {
          changeServer();
        }
    });
    $("button#available").click(function(){
        changeState(true);
        connect();
    });
    $("button#unavailable").click(function(){
        changeState(false);
        connect();
    });
    $("text").click(function(){
        changeServer();
    });
    $("button#search").click(function(){
        getData();
    });
    $("input#days").on('keypress',function (e){
        if (e.key === 'Enter') {
          getData();
        }
    });
});
function changeServer(){
    const newVal = $("section#server input").val();
    if(newVal !== "" && newVal !== undefined){
        server = newVal;
        $("section#server p>strong").text(newVal);
        connect();
    }
}
function getData(){
    const days = $("input#days").val();        
    if(data === "" || data === undefined || !$.isNumeric(days) || days<=0){
        return;
    }   
    $.getJSON(server+"?request=data&days="+days)
    .done(function(results){
        const tbody =  $("table tbody");
        tbody.empty();
        $("table caption").html(`Data of last <strong>${days}</strong> days. Number of deposits: <strong>${results.data.length}</strong>`);
        tbody
        .append($("<tr>")
            .append($("<th>",{text:"Time",id:"time",span:"col"}))
            .append($("<th>",{text:"Quantity",id:"quantity",span:"col"})));
        
        $.each(results.data,function(i,elem){
            drawLine(tbody,elem);
        });
    })
    .fail(function(){
        printError("ciao,sei un fallito");
    });
}
function drawLine(tbody,elem){    
    tbody.append($("<tr>")
        .append($("<td>",{headers:"time",text:new Date(elem.time).toLocaleString("it-IT")}))
        .append($("<td>",{headers:"quantity",text:elem.quantity})));
}
function connect(){
    $("section#server div p#error").remove();
    $("section#server input").val("");
    $.getJSON(server+"?request=state").done(function(result){
        $("section#management strong").text(function(i,text){
            return result.state? "Available":"Unavailable";
        });
        
        $("li#deposits strong").text(result.deposits);
        $("li#quantity strong").text(result.quantity);
        $("li#available strong").text(result.available);
    }).fail(function(){
        $("section#server div").append($("<p>",{text:"Error in the connection",id:"error"}));        
    });
}
function changeState(state){
    $.post({
        url: server+stateUrl,
        data: JSON.stringify({"state":state}),
        dataType: "json"
    }).fail(function(){
        printError("Impossibile cambiare stato");
    });
}
