#include "DumpsterEdge.h"

DumpsterEdge::DumpsterEdge(int ledAvailable, int ledNotAvailable, int pot,bool state) { 
	this->ledAvailable = new Led(ledAvailable);
    this->ledNotAvailable = new Led(ledNotAvailable);
    this->pot = new Potentiometer(pot);
    this->init(state);
}
int DumpsterEdge::dump() {
    if(this->checkState()){
        int newValue = this->pot->getMappedValue(INITIAL_VALUE,MAX_VALUE);
        if(newValue > this->tot+INTERVAL){
            int temp = newValue-this->tot;
            this->tot = newValue;
            if(this->tot >= MAX_VALUE){
                this->changeState(false);
            }
            return temp;
        }
    }
    return -1;
}

void DumpsterEdge::changeState(bool state){
    this->state = state;
    if(this->state){
        this->ledNotAvailable->switchOff();
        this->ledAvailable->switchOn();
    }
    else{
        this->ledAvailable->switchOff();
        this->ledNotAvailable->switchOn();
    }
}

bool DumpsterEdge::checkState() {
    return this->state;
}

void DumpsterEdge::init(bool state) {
    if(state){
        this->tot = 0;
    }
    this->changeState(state);
}

int DumpsterEdge::getSpaceLeft() {
    return MAX_VALUE-this->tot;
}