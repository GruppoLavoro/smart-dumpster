#ifndef _DUMPSTER_EDGE_H_
#define _DUMPSTER_EDGE_H_
#include "Led.h" 
#include "Potentiometer.h"
#define INITIAL_VALUE 0
#define MAX_VALUE 100
#define INTERVAL 1

class DumpsterEdge{  
	private:
		Led* ledAvailable;
    Led* ledNotAvailable;
    Potentiometer* pot;
    int tot;
    bool state;
    void changeState(bool state);
	public:  
		DumpsterEdge(int ledAvailable, int ledNotAvailable, int pot,bool state);
		int dump();
    bool checkState();
    int getSpaceLeft();
    void init(bool state);
};
#endif
