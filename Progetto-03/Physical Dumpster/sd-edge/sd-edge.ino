#include <ESP8266mDNS.h>
#include <ESP8266mDNS_Legacy.h>
#include <LEAmDNS.h>
#include <LEAmDNS_lwIPdefs.h>
#include <LEAmDNS_Priv.h>

#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include "Utils.h"
#include "DumpsterEdge.h"
#define ERROR -1
/* wifi network name */
char* netName = "daniela19";
/* WPA2 PSK password */
char* pwd = "distinzi";
/* service IP address */ 
char* address = "http://1854c0d3.ngrok.io/";
DumpsterEdge* dumpster;

void setup() { 
  Serial.begin(115200);                                
  WiFi.begin(netName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
   int state = requestStatus(address);
   if(state!=ERROR){
    dumpster = new DumpsterEdge(LED_AVAILABLE,LED_NOT_AVAILABLE,POT,state);
   }
}

void sendData(String address, int value){  
   HTTPClient http;    
   http.begin(address + "/api/data");      
   http.addHeader("Content-Type", "application/json");     
   String msg = "{ \"quantity\": " + String(value) +" }";
   http.end();        
}

void sendSpaceLeft(String address, int value){  
   HTTPClient http;    
   http.begin(address + "/api/available");      
   http.addHeader("Content-Type", "application/json");     
   String msg = "{ \"available\": " + String(value) +" }";
   http.end();        
}

void sendState(String address, bool state){  
   HTTPClient http;    
   http.begin(address + "/api/state");      
   http.addHeader("Content-Type", "application/json");
   String msg;
   if(state){
    msg = "{ \"state\": true }";
   }
   else{
    msg = "{ \"state\": false }";
   }
   http.end();    
}

int requestStatus(String address){  
   HTTPClient http;     
   http.begin(address + "/?request=state"); 
   int retCode = http.GET(); 
   int state = getStateFromJson(http.getString()); 
   http.end(); 
   if (retCode != 200){
    return ERROR;
   }    
   return state;
}

int getStateFromJson(String msg){
  StaticJsonDocument<1000> jsonBuffer;
  char charArr[msg.length()];
  msg.toCharArray(charArr,msg.length());
  bool error = deserializeJson(jsonBuffer,charArr);
  if(!error){
    return ERROR;
  }
  return jsonBuffer["state"];
}

void loop() { 
 if (WiFi.status()== WL_CONNECTED){   
   int state = requestStatus(address);
   if(state!=ERROR&&state!=dumpster->checkState()){
    dumpster->init(state);
   }
   int value = dumpster->dump();
   state = dumpster->checkState();
   int spaceLeft = dumpster->getSpaceLeft();
   /* send data */
   if(value!=ERROR){
     Serial.println("sending "+String(value)+"...");    
     sendData(address, value);
     sendState(address, state);
     sendSpaceLeft(address, spaceLeft);
   }
 } else { 
    Serial.println("Error in WiFi connection");   
 }
 delay(500);  
}
