#include <SoftwareSerial.h>
/*
* BT module connection:
* - pin 2 <=> TXD
* - pin 3 <=> RXD
*
*/
SoftwareSerial btChannel(2, 3);
void setup() {
Serial.begin(38400);
while (!Serial) {};
btChannel.begin(38400);
}
void loop() {
/* reading data from BT to Serial */
if (btChannel.available())
Serial.write(btChannel.read());
/* reading data from the Serial to BT */
if (Serial.available())
btChannel.write(Serial.read());
}
