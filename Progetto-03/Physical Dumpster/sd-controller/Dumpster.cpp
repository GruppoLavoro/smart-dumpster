#include "Dumpster.h"

Dumpster::Dumpster(int led_a, int led_b, int led_c,int servo) { 
	  this->ledA = new Led(led_a);
    this->ledB = new Led(led_b);
    this->ledC = new Led(led_c);
    this->servo = new Servo();
    this->servo->attach(servo);
    this->state = CLOSED;
    this->timer = TIME+millis();
}
void Dumpster::dump(StateType state) {
    if(state == EXTEND){
      this->timer += TIME;  
      return;
    }
    if(this->state != state){
        this->close(this->state);
    }
    if(this->state == CLOSED){
        this->state = state;
        this->timer = TIME + millis();
        switch (state){
        case TYPE_A:
            this->ledA->switchOn();
            break;
        case TYPE_B:
            this->ledB->switchOn();
            break;
        case TYPE_C:
            this->ledC->switchOn();
            break;
        default:
            break;
        }
        this->servo->write(OPEN_POS);
      
    }
    return;
}
void Dumpster::close(StateType state){
	  switch (state){
        case TYPE_A:
            this->ledA->switchOff();
            break;
        case TYPE_B:
            this->ledB->switchOff();
            break;
        case TYPE_C:
            this->ledC->switchOff();
            break;
        default:
            break;
    }
    this->state = CLOSED;
    this->servo->write(CLOSED_POS);
}

void Dumpster::check(){

  if(millis()>=this->timer){
     this->close(this->state);
  }
}
