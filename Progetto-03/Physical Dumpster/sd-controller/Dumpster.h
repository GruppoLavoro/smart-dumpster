#ifndef _DUMPSTER_H_
#define _DUMPSTER_H_
#include "Led.h" 
#include <Servo.h>
#include "Utils.h"
#define TIME 10000
class Dumpster{  
	private:
        StateType state;
		    Led* ledA;
        Led* ledB;
        Led* ledC;
        Servo* servo;
        unsigned long timer;
        void close(StateType type);
	public:  
		Dumpster(int led_a, int led_b, int led_c,int servo);
		void dump(StateType type);
    void check();
};
#endif
