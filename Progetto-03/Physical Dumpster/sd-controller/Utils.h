#ifndef _UTILS_H_
#define _UTILS_H_
/*      pins        */
#define LED_A 12
#define LED_B 11
#define LED_C 10
#define SERVO 6
#define CLOSED_POS 0
#define OPEN_POS 180
#define TX 2
#define RX 3

enum StateType{
    TYPE_A,
    TYPE_B,
    TYPE_C,
    EXTEND,
    CLOSED
};

#endif
