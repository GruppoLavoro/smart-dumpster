#include <SoftwareSerial.h>
#include "Utils.h"
#include "Dumpster.h"
/*  *  BT module connection:
*  - pin 2 <=> TXD
*  - pin 3 <=> RXD  *
*/
#define DELAY 100
SoftwareSerial btChannel(TX, RX);
Dumpster* dumpster;
void setup() {
  btChannel.begin(9600);
  Serial.begin(9600);
  dumpster = new Dumpster(LED_A,LED_B,LED_C,SERVO);
}
void loop() {
  StateType input;
  if(btChannel.available()){
    input = commandConverter(btChannel.readString());
    dumpster->dump(input);
  }
  dumpster->check();
  delay(DELAY);
} 

StateType commandConverter(String command){
  Serial.print(command);
  if(command == "A"){
      return TYPE_A;
  }else if(command == "B"){
      return TYPE_B;
  }else if(command == "C"){
      return TYPE_C;
  }else if(command == "E"){
      return EXTEND;
  }else{
      return CLOSED;
  }
}
