package launcher;

import java.io.IOException;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import server.DataService;

public class MainLauncher {
	public static void main(final String[] args) throws IOException {
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(8080);
		vertx.deployVerticle(service, new DeploymentOptions().setWorker(true));
	}
}
