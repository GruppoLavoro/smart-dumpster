package io;

import java.io.IOException;

import io.vertx.core.json.JsonObject;

public interface JsonIO {

	void writeJson(JsonObject json) throws IOException;

	JsonObject getJson() throws IOException;
}
