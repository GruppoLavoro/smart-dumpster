package io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import io.vertx.core.json.JsonObject;

public class JsonIOImpl implements JsonIO {
	private final String path;

	public JsonIOImpl(final String path) {
		this.path = path;
	}

	@Override
	public void writeJson(final JsonObject json) throws IOException {
		this.checkFile();
		Objects.requireNonNull(json);
		try (FileWriter writer = new FileWriter(this.path)) {
			writer.write(json.encode());
		}
	}

	@Override
	public JsonObject getJson() throws IOException {
		this.checkFile();
		return new JsonObject(new String(Files.readAllBytes(Paths.get(this.path))));
	}

	private void checkFile() throws IOException {
		final File file = new File(this.path);
		if (!file.exists()) {
			file.createNewFile();
			try (FileWriter writer = new FileWriter(this.path)) {
				writer.write(new JsonObject().encodePrettily());
			}
		}
	}

}
