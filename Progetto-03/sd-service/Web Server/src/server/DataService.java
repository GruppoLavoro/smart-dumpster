package server;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.JsonIO;
import io.JsonIOImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class DataService extends AbstractVerticle {

	private static final boolean INITIAL_STATE = true;
	private static final long DAY_IN_MS = 86400000;
	private static final String PATH = "./data.json";
	private static final String DATA_KEY = "data";
	private static final String STATE_KEY = "state";
	private static final String AVAILABLE_KEY = "available";
	private final int port;

	public DataService(int port) throws IOException {
		this.port = port;
	}

	@Override
	public void start() {
		final Router router = Router.router(vertx);

		final Set<String> allowedHeaders = new HashSet<>();
		allowedHeaders.add("Content-Type");

		final Set<HttpMethod> allowedMethods = new HashSet<>();
		allowedMethods.add(HttpMethod.GET);
		allowedMethods.add(HttpMethod.POST);

		router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));
		router.route().handler(BodyHandler.create());
		router.post("/api/data").handler(this::handlePostData);
		router.post("/api/state").handler(this::handlePostState);
		router.post("/api/available").handler(this::handlePostAvailable);
		router.get().handler(this::handleGet);
		vertx.createHttpServer().requestHandler(router::accept).listen(port);

		log("Service ready.");
	}

	private void handlePostState(final RoutingContext routingContext) {
		final Boolean newState = routingContext.getBodyAsJson().getBoolean(STATE_KEY);
		if (newState == null) {
			return;
		}
		try {
			final JsonIO jsonIO = new JsonIOImpl(PATH);
			final JsonObject fileContent = jsonIO.getJson();
			this.checkJsonFields(fileContent);
			fileContent.put(STATE_KEY, newState);
			jsonIO.writeJson(fileContent);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void handlePostAvailable(final RoutingContext routingContext) {
		final Integer response = routingContext.getBodyAsJson().getInteger(AVAILABLE_KEY);
		if (response == null) {
			return;
		}
		try {
			final JsonIO jsonIO = new JsonIOImpl(PATH);
			final JsonObject fileContent = jsonIO.getJson();
			this.checkJsonFields(fileContent);
			fileContent.put(AVAILABLE_KEY,response);
			jsonIO.writeJson(fileContent);
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void handlePostData(final RoutingContext routingContext) {
		final JsonObject response = routingContext.getBodyAsJson();
		if (response == null) {
			return;
		}
		try {
			final JsonIO jsonIO = new JsonIOImpl(PATH);
			final JsonObject fileContent = jsonIO.getJson();
			this.checkJsonFields(fileContent);
			response.put("time", System.currentTimeMillis());
			fileContent.getJsonArray(DATA_KEY).add(response);
			jsonIO.writeJson(fileContent);
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void handleGet(final RoutingContext routingContext) {
		final String request = routingContext.request().getParam("request");
		final String daysString = routingContext.request().getParam("days");
		final JsonObject reply = new JsonObject();
		boolean response = false;
		if (request == null || (request.equals(DATA_KEY) && daysString == null)) {
			response = false;
		} else {
			try {
				final JsonIO jsonIO = new JsonIOImpl(PATH);
				final JsonObject fileContent = jsonIO.getJson();
				this.checkJsonFields(fileContent);
				final Stream<Object> jsonStream = fileContent.getJsonArray("data").stream();

				if (request.equals(STATE_KEY)) {
					final boolean state = fileContent.getBoolean(STATE_KEY);
					final int available = fileContent.getInteger(AVAILABLE_KEY);
					reply.put(STATE_KEY, state);
					reply.put(AVAILABLE_KEY, available);
					reply.put("deposits", fileContent.getJsonArray("data").size());
					reply.put("quantity", jsonStream.mapToInt(e -> ((JsonObject) e).getInteger("quantity")).sum());
					response = true;
				}
				if (request.equals(DATA_KEY)) {
					final int days = Integer.parseInt(routingContext.request().getParam("days"));
					final long time = this.getPastMillis(days);
					final JsonArray deposits = new JsonArray(
							jsonStream.map(e -> (JsonObject) e).filter(l -> l.getLong("time") >= time)
									.sorted((a, b) -> Long.compare(b.getLong("time"), a.getLong("time")))
									.collect(Collectors.toList()));
					reply.put(DATA_KEY, deposits);
					response = true;
				}
			} catch (IOException e) {
				reply.clear();
			}
		}
		reply.put("response", response);
		routingContext.response().putHeader("content-type", "application/json").end(reply.encodePrettily());

	}

	private void checkJsonFields(final JsonObject fileContent) throws IOException {
		final JsonIO jsonIO = new JsonIOImpl(PATH);
		final Boolean state = fileContent.getBoolean(STATE_KEY);
		final Integer available = fileContent.getInteger(AVAILABLE_KEY);
		final JsonArray data = fileContent.getJsonArray(DATA_KEY);

		if (state == null) {
			fileContent.put(STATE_KEY, INITIAL_STATE);
		}
		if (available == null) {
			fileContent.put(AVAILABLE_KEY, 0);
		}
		if (data == null) {
			fileContent.put(DATA_KEY, new JsonArray());
		}

		jsonIO.writeJson(fileContent);
	}
	private void log(final String msg) {
		System.out.println("[DATA SERVICE] " + msg);
	}

	private long getPastMillis(final int days) {
		return System.currentTimeMillis() - DAY_IN_MS * days;
	}
}