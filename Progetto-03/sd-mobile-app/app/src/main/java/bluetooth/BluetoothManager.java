package bluetooth;

import android.bluetooth.BluetoothDevice;

import java.util.UUID;


public class BluetoothManager{

    private BluetoothChannel btChannel;
    private Listener listener;
    private boolean connectionActive;

    public BluetoothManager(final Listener listener) {
        this.listener = listener;
        this.connectionActive = false;
        try {
            this.connectToBTServer();
        } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
            bluetoothDeviceNotFound.printStackTrace();
        }
    }

    public void sendMessage(final String message) {
        this.btChannel.sendMessage(message);
    }

    public boolean isConnectionActive(){
        return this.connectionActive;
    }

    public void stop() {
        btChannel.close();
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);

        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();
        //final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                listener.onConnectionSuccess();
                connectionActive = true;
                btChannel = channel;

                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        listener.onMessageReceived(receivedMessage);
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {}
                });
            }

            @Override
            public void onConnectionCanceled() {
                btChannel = null;
                connectionActive = false;
                listener.onConnectionCanceled();
            }
        }).execute();
    }

    public interface Listener{
        void onMessageReceived(String message);
        void onConnectionSuccess();
        void onConnectionCanceled();
    }
}
