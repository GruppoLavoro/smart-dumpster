package bluetooth;

public class C {

    public static final String APP_LOG_TAG = "BT CLN";
    static final String LIB_TAG = "BluetoothLib";

    class channel {
        static final int MESSAGE_RECEIVED = 0;
        static final int MESSAGE_SENT = 1;
    }

    class message {
        static final char MESSAGE_TERMINATOR = '\0';
    }

    public class bluetooth {
        public static final int ENABLE_BT_REQUEST = 1;
        static final String BT_DEVICE_ACTING_AS_SERVER_NAME = "beimax";
        public static final String BT_SERVER_UUID = "7ba55836-01eb-11e9-8eb2-f2801f1b9fd1";
    }
}
