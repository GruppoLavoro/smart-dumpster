package bluetooth;

public interface ExtendedRunnable extends Runnable {
    void write(byte[] bytes);
    void cancel();
}
