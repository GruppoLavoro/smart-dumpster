package net;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class HttpResponse {
    private int code;
    private InputStream response;

    HttpResponse(final int code, final InputStream response){
        this.code = code;
        this.response = response;
    }

    public int code(){
        return code;
    }

    public InputStream content(){
        return response;
    }

    public JSONObject contentAsJSON() throws IOException {
        try{
            JSONObject json = new JSONObject(IOUtils.toString(response, StandardCharsets.UTF_8));
            response.close();
            return json;
        }catch (JSONException e){
            return null;
        }
    }
}
