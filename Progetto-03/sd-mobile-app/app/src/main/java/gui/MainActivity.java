package gui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sd_mobile_app.R;

import org.json.JSONException;

import bluetooth.BluetoothManager;
import bluetooth.C;
import controller.Controller;
import controller.ControllerImpl;

public class MainActivity extends AppCompatActivity {
    private String btStatusLabel;
    private Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btStatusLabel = getString(R.string.bluetoothLabel);

        this.controller = new ControllerImpl();
        this.setUp();
    }
    private void setUp(){
        this.checkButtonsEnablement();
        this.enableOrCreateBt();
        this.attachListeners();
    }
    private void enableOrCreateBt(){
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter != null && !btAdapter.isEnabled()){
            this.enableBluetooth();
        }else{
            this.createBluetooth();
        }
    }
    private void attachListeners(){
        // server data request
        findViewById(R.id.token).setOnClickListener(l-> {
            final String url = ((EditText)findViewById(R.id.server)).getText().toString();
            this.controller.requireToken(url, new Controller.TokenListener() {
                final private TextView serverStatus = findViewById(R.id.serverStatusToken);
                @Override
                public void setToken(Boolean response) {
                    if (response) {
                        serverStatus.setText("Server: Got the Token!");
                        checkButtonsEnablement();
                    } else {
                        serverStatus.setText("Server: Token Unavailable");
                    }
                }
                @Override
                public void onJSONException() {
                    serverStatus.setText("Token currupted!");
                }

                @Override
                public void onConnectionError() {
                    serverStatus.setText("Connection Error");
                }
            });
        });
        final TextView serverStatus = (TextView)findViewById(R.id.serverStatusToken);
        final View.OnClickListener dumpsterButtonListener = l ->{
            this.checkIfTokenIsPresentAndSend(((Button)findViewById(l.getId())).getText().toString());
            this.controller.removeToken();
            this.checkButtonsEnablement();
        };

        findViewById(R.id.buttonA).setOnClickListener(dumpsterButtonListener);
        findViewById(R.id.buttonB).setOnClickListener(dumpsterButtonListener);
        findViewById(R.id.buttonC).setOnClickListener(dumpsterButtonListener);
        findViewById(R.id.buttonExtend).setOnClickListener(l->{
            this.controller.sendMessage("E");
        });
        findViewById(R.id.btConnect).setOnClickListener(l->{
            this.enableOrCreateBt();
        });
    }
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data){
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED){
            this.enableBluetooth();
        }
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK){
            this.createBluetooth();
        }
    }
    @Override
    public void onDestroy(){
        this.controller.stopBluetooth();
        super.onDestroy();
    }
    private void enableBluetooth(){
        this.controller.stopBluetooth();
        this.startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
    }
    private void createBluetooth() {
        if(this.controller.isBTCreated() && this.controller.getBluetoothStatus()){
            return;
        }
        this.controller.setBluetoothListener(new BluetoothManager.Listener() {
            @Override
            public void onMessageReceived(final String message) { }

            @Override
            public void onConnectionSuccess() {
                runOnUiThread(()->((TextView)findViewById(R.id.bluetoothStatus)).setText(btStatusLabel+" Connected"));
            }

            @Override
            public void onConnectionCanceled() {
                controller.stopBluetooth();
                runOnUiThread(()->((TextView)findViewById(R.id.bluetoothStatus)).setText(btStatusLabel+" Disconnected"));
            }
        });
    }

    private void checkIfTokenIsPresentAndSend(String message){
        if(this.controller.isTokenAvailable()) {
            this.controller.sendMessage(message);
            ((TextView)findViewById(R.id.serverStatusToken)).setText(getString(R.string.tokenLabel));
        }else{
            Toast.makeText(getApplicationContext(),"Get token first!",Toast.LENGTH_SHORT).show();
        }
    }
    private void setCommandButtons(final boolean state){
        findViewById(R.id.buttonA).setEnabled(state);
        findViewById(R.id.buttonB).setEnabled(state);
        findViewById(R.id.buttonC).setEnabled(state);
    }
    private void checkButtonsEnablement(){
        if(this.controller.getBluetoothStatus() && this.controller.isTokenAvailable()){
            this.setCommandButtons(true);
        }else{
            this.setCommandButtons(false);
        }
    }

}
