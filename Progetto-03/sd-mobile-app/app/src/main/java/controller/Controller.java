package controller;

import org.json.JSONException;
import org.json.JSONObject;

import bluetooth.BluetoothManager;

public interface Controller {

    public interface TokenListener{
        void setToken(final Boolean response);
        void onJSONException();
        void onConnectionError();
    }

    void requireToken(String url,TokenListener listener);
    void setBluetoothListener(final BluetoothManager.Listener listener);
    void sendMessage(final String message);
    void stopBluetooth();
    boolean getBluetoothStatus();
    boolean isBTCreated();
    boolean isTokenAvailable();
    void removeToken();
}


