package controller;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Optional;

import net.Http;

import org.json.JSONException;
import org.json.JSONObject;

import bluetooth.BluetoothManager;

public class ControllerImpl implements Controller {

    private static final String GET_VALUES = "?request=state";
    private BluetoothManager btManager;
    private boolean token;


    public ControllerImpl(){
        this.btManager = null;
        this.token = false;
    }
    @Override
    public void requireToken(final String url,final TokenListener listener){
        Http.get(url+GET_VALUES, response -> {
            if(response == null){
                listener.onConnectionError();
            }else if(response.code() == HttpURLConnection.HTTP_OK){
                try{
                    final JSONObject result = response.contentAsJSON();
                    this.token = result.getBoolean("state");
                    listener.setToken(this.token);
                }catch(IOException e){
                    listener.onConnectionError();
                }catch(JSONException e){
                    listener.onJSONException();
                }

            }
        });
    }
    @Override
    public void setBluetoothListener(final BluetoothManager.Listener listener){
        this.btManager = new BluetoothManager(listener);

    }
    @Override
    public void stopBluetooth(){
        if(this.btManager != null && this.btManager.isConnectionActive()){
            this.btManager.stop();
            this.btManager = null;
        }
    }
    @Override
    public void sendMessage(final String message){
        if(this.btManager != null && this.btManager.isConnectionActive() && this.token){
            this.btManager.sendMessage(message);
        }
    }
    @Override
    public boolean getBluetoothStatus(){
        return this.btManager != null && this.btManager.isConnectionActive();
    }
    @Override
    public boolean isBTCreated(){
        return  this.btManager != null;
    }
    @Override
    public boolean isTokenAvailable(){
        return this.token;
    }
    @Override
    public void removeToken(){
        this.token = false;
    }
}
